# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Classes describing events detected in camera stream.

All classes use the same base class Event, which contains all information,
which means that the Event subclasses are mainly for convenience and do not
add any further functionality.
"""
from safetynet import TypecheckMeta, Optional

class Event(object):
  """Describes a generic event detected in the camera stream."""
  __metaclass__ = TypecheckMeta

  EVENT_TYPE_NAME = None

  STATE_ON = True
  STATE_OFF = False
  STATE_WHITE = False
  STATE_BLACK = True
  STATE_CLOSED = False
  STATE_OPEN = True

  def __init__(self, time, location=None, start_time=None, state=None,
               analog_state=None):
    """
    :param int time
    :param Optional[float] location
    :param Optional[int] start_time
    :param Optional[bool] state
    """
    self.time = time
    self.location = float(location) if location is not None else None
    self.start_time = start_time
    self.state = state
    self.analog_state = analog_state

    self.uncalib_time = time
    self.uncalib_start_time = start_time
    self.uncalib_location = location

  def __setstate__(self, state):
    """Make sure we can read legacy pickled files.

    Previous versions used to store the raw uncalibrated info in time,
    start_time, location and state. Also we want to make sure the constructor
    is called to initialize the Event correctly.
    """
    time = state.get("uncalib_time", state.get("time"))
    location = state.get("uncalib_location", state.get("location"))
    start_time = state.get("uncalib_start_time", state.get("start_time"))
    event_state = state.get("state")
    Event.__init__(self, time, location, start_time, event_state)
    self.__dict__.update(state)

  def __repr__(self):
    optional = []
    if self.location is not None:
      optional.append("location=%.1f" % self.location)
    if self.start_time is not None:
      optional.append("start_time=%d" % self.start_time)
    if self.state is not None:
      optional.append("state=On" if self.state else "state=Off")

    optional_str = ""
    if optional:
      optional_str = " (%s)" % (", ".join(optional))
    return "%d\t%s\t%s" % (self.time, self.EVENT_TYPE_NAME, optional_str)


class LineDrawEvent(Event):
  EVENT_TYPE_NAME = "LineDraw"

  def __init__(self, time, location, start_time):
    """
    :param int time
    :param Optional[float] location
    :param int start_time
    """
    Event.__init__(self, time, location=location, start_time=start_time)


class FingerEvent(Event):
  EVENT_TYPE_NAME = "Finger"

  def __init__(self, time, location):
    """
    :param int time
    :param float location
    """
    Event.__init__(self, time, location=location)


class ScreenDrawEvent(Event):
  EVENT_TYPE_NAME = "ScreenDraw"

  def __init__(self, time, start_time, state):
    """
    :param int time
    :param int start_time
    :param bool state
    """
    Event.__init__(self, time, start_time=start_time, state=state)


class StateChangeEvent(Event):
  EVENT_TYPE_NAME = "StateChange"

  def __init__(self, time, start_time, state):
    """
    :param int time
    :param int start_time
    :param bool state
    """
    Event.__init__(self, time, start_time=start_time, state=state)


class LEDEvent(Event):
  EVENT_TYPE_NAME = "LED"

  def __init__(self, time, state):
    """
    :param int time
    :param bool state
    """
    Event.__init__(self, time, state=state)


class AnalogStateEvent(Event):
  EVENT_TYPE_NAME = "AnalogState"

  def __init__(self, time, analog_state):
    """
    :param int time
    :param float analog_state
    """
    Event.__init__(self, time, analog_state=analog_state)

