# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Start the apex-monitor"
author        "chromium-os-dev@chromium.org"

start on starting system-services
stop on stopping system-services
respawn
respawn limit 3 10

pre-start script
  BOARD=$(grep CHROMEOS_RELEASE_BOARD /etc/lsb-release | cut -d '=' -f 2)
  OEM=$(mosys platform sku)
  i2c_bus=$(ls /sys/bus/pci/devices/0000:00:15.2/i2c_designware.1 | grep i2c)
  if [ ${BOARD} != fizz -o ${OEM} != 6 ]; then
    # If not Bleemo device, stop here.
    stop
    exit 0
  fi
  # Configure power control gpios.
  echo 404 > /sys/class/gpio/export
  echo 416 > /sys/class/gpio/export
  echo 417 > /sys/class/gpio/export
  echo out > /sys/class/gpio/gpio404/direction
  echo 1 > /sys/class/gpio/gpio404/value
  echo out > /sys/class/gpio/gpio416/direction
  echo 1 > /sys/class/gpio/gpio416/value
  echo out > /sys/class/gpio/gpio417/direction
  echo 1 > /sys/class/gpio/gpio417/value
  chown cfm-monitor /sys/class/gpio/gpio404/direction
  chown cfm-monitor /sys/class/gpio/gpio404/value
  chown cfm-monitor /sys/class/gpio/gpio416/direction
  chown cfm-monitor /sys/class/gpio/gpio416/value
  chown cfm-monitor /sys/class/gpio/gpio417/direction
  chown cfm-monitor /sys/class/gpio/gpio417/value
  # Load driver for i2c mux on dc1.
  echo pca9546 0x70 > /sys/bus/i2c/devices/${i2c_bus}/new_device
  # Sleep 1s to allow the device to be fully loaded.
  sleep 1
  # Change permission and group for the i2c buses
  # so they can be accessed within minijail.
  chmod 0660 /dev/${i2c_bus}
  chown :cfm-monitor /dev/${i2c_bus}
  ls /sys/bus/i2c/devices/${i2c_bus}/ | grep i2c-[0-9] | while read -r bus
  do
    chmod 0660 /dev/${bus}
    chown :cfm-monitor /dev/${bus}
  done
end script

post-stop script
  BOARD=$(grep CHROMEOS_RELEASE_BOARD /etc/lsb-release | cut -d '=' -f 2)
  OEM=$(mosys platform sku)
  if [ ${BOARD} = fizz -o ${OEM} = 6 ]; then
    echo 404 > /sys/class/gpio/unexport
    echo 416 > /sys/class/gpio/unexport
    echo 417 > /sys/class/gpio/unexport
  fi
end script

expect fork

# -e Enter new network namespace.
# -i Exit immediately after fork (do not act as init).
# -l Enter new IPC namespace.
# -p Enter new pid namespace
# -n Set no_new_privs
# -t Create a tmpfs for /tmp
# Mount /sys, /dev so that gpio can be toggled and i2c device and be accessed.
exec minijail0 -u cfm-monitor -g cfm-monitor \
  -e -i -l -p -n -t \
  -P /mnt/empty -b / -b /sys -b /dev,/dev,0 \
  -- /usr/sbin/apex-monitor
