# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description   "Mount and setup moblab external storage."
author        "chromium-os-dev@chromium.org"

start on started cros-disks

normal exit 0

env MOUNT_DIR=/mnt/moblab

env STATIC_DIR=/mnt/moblab/static

env MIN_STATEFUL_DISKSPACE=32000000
env LOCAL_STORAGE_DIR=/usr/local/moblab-storage

script
  mkdir -p /var/log/bootup/
  exec >>/var/log/bootup/${UPSTART_JOB}.log 2>&1
  set -x
  set -e
  logger -t "${UPSTART_JOB}" "Starting"

  # REQUIRES A USB DRIVE WITH LABEL "MOBLAB-STORAGE"
  local_part=$(dirname "${LOCAL_STORAGE_DIR}")
  diskspace=$(df "${local_part}" | awk '!/Filesystem/ { print $4 }')
  # It has been observed readlink fails occasionally ( typically permission
  # denied ), report the error messages from readlink but continue as if no
  # usb drive has been found.
  sleep 1
  usb_drive=$(readlink -v -f /dev/disk/by-label/MOBLAB-STORAGE) ||
      echo "Readlink failed code $?"
  if [ ! -e "${usb_drive}" ] &&
         [ ${diskspace} -lt ${MIN_STATEFUL_DISKSPACE} ]; then
    sleep 5
    logger -t "${UPSTART_JOB}" "Retrying search for usb disk"
    # This may force a rescan of all the USB devices.
    lsusb -v
    usb_drive=$(readlink -v -f /dev/disk/by-label/MOBLAB-STORAGE) ||
        echo "Readlink failed code $?"
  fi
  if [ -e "${usb_drive}" ]; then
    logger -t "${UPSTART_JOB}" "Mounting external storage using ${usb_drive}."
    umount -A "${usb_drive}" || :
    /sbin/e2fsck -p "${usb_drive}" || :
    if ! mount "${usb_drive}" "${MOUNT_DIR}" -o exec,dev,nosuid; then
      logger -t "${UPSTART_JOB}" "Mounting of ${usb_drive} onto ${MOUNT_DIR}"
      logger -t "${UPSTART_JOB}" "failed. Please reconnect drive and reboot."
      exit 1
    fi
  else
    logger -t "${UPSTART_JOB}" "No disk labeled MOBLAB-STORAGE found!"
    if [ ${diskspace} -lt ${MIN_STATEFUL_DISKSPACE} ]; then
      logger -t "${UPSTART_JOB}" "Moblab will not launch. Please insert a"
      logger -t "${UPSTART_JOB}" "properly configured drive and reboot."
      exit 1
    fi
    logger -t "${UPSTART_JOB}" "Moblab will launch using internal disk space."
    mkdir -p "${LOCAL_STORAGE_DIR}"
    logger -t "${UPSTART_JOB}" "Remounting /usr/local/ with dev permissions."
    mount -o remount,dev /usr/local
    mount --bind "${LOCAL_STORAGE_DIR}" "${MOUNT_DIR}"
  fi

  mkdir -p "${STATIC_DIR}"
  chown moblab:moblab "${MOUNT_DIR}" || :
  chown -R moblab:moblab "${MOUNT_DIR}/static" || :

  logger -t "${UPSTART_JOB}" "Ending."
end script
