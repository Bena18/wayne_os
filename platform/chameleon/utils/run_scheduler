#!/usr/bin/env python
# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Run scheduler daemon."""

import argparse
import logging
import schedule
import time

from chameleond.utils import file_utils
from chameleond.utils import network_utils


def Main():
  """The main program, to run scheduler daemon."""
  parser = argparse.ArgumentParser(
      description='Launch a scheduler daemon.',
      formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--network_period_secs', type=float, default=10,
                      help='Period in seconds to check network.')

  args = parser.parse_args()

  SetNetworkSchedule(args.network_period_secs)
  ClearObsoleteFiles()

  Run()


def Run():
  """Runs scheduler."""
  while True:
    schedule.run_pending()
    time.sleep(1)


def SetNetworkSchedule(period_secs):
  """Sets the networking schedule.

  Args:
    period_secs: Period in seconds to check network status.
  """
  schedule.every(period_secs).seconds.do(network_utils.PossiblyRestartNetwork)


# A list of files whose content we do not care.
OBSOLETE_FILES = ['/www/logs/lighttpd.error.log']
# The period in seconds to clear the content of the files.
CLEAR_PERIOD_SECS = 3600

def ClearObsoleteFiles():
  """Clears the content of obsolete files.

  Note that we do not remove the files in case system expects the existence
  of the files.
  """

  def _ClearFiles():
    for f in OBSOLETE_FILES:
      file_utils.TruncateToZero(f)

  schedule.every(CLEAR_PERIOD_SECS).seconds.do(_ClearFiles)


if __name__ == '__main__':
  # schedule module uses logging.info when it does a scheduled work so it is
  # to chatty.
  logging.basicConfig(
      level=logging.WARNING,
      format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  Main()
