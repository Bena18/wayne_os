Kūrėjo parinktys
Rodyti derinimo informaciją
Įgalinti OS tikrinimą
Išjungti
Kalba
Paleisti iš tinklo
Paleisti pasenusią BIOS sistemą
Paleisti iš USB
Paleidimas iš USB arba SD kortelės
Paleisti iš vidinio disko
Atšaukti
Patvirtinti OS tikrinimo įgalinimą
Išjungti OS tikrinimą
Patvirtinti OS tikrinimo išjungimą
Naudokite garsumo mygtukus, kad naršytumėte aukštyn arba žemyn,
ir maitinimo mygtuką, kad pasirinktumėte parinktį.
Jei išjungsite OS patvirtinimą, sistema taps NESAUGI.
Pasirinkite „Atšaukti“, kad sistema būtų apsaugota.
OS patvirtinimas IŠJUNGTAS. Sistema yra NESAUGI.
Pasirinkite „Įgalinti OS tikrinimą“, kad sistema vėl būtų saugi.
Pasirinkite „Patvirtinti OS tikrinimo įgalinimą“, kad apsaugotumėte sistemą.
