Utvecklaralternativ
Visa felsökningsinformation
Aktivera verifiering av operativsystemet
Stäng av
Språk
Starta från nätverk
Starta Legacy BIOS
Starta från USB
Starta från en USB-enhet eller ett SD-kort
Starta från intern disk
Avbryt
Bekräfta aktivering av verifiering av operativsystemet
Inaktivera verifiering av operativsystemet
Bekräfta inaktivering av verifiering av operativsystemet
Navigera uppåt eller nedåt med volymknapparna
och välj ett alternativ genom att trycka på strömbrytaren.
Om du inaktiverar verifiering av operativsystemet är systemet INTE LÄNGRE SÄKERT.
Välj Avbryt om du vill behålla skyddet.
Verifiering av operativsystemet är INAKTIVERAD. Systemet är INTE SÄKERT.
Välj Aktivera verifiering av operativsystemet för att återfå skyddet.
Välj Bekräfta aktivering av verifiering av operativsystemet för att skydda systemet.
