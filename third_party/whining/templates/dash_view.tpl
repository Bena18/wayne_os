%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root


%def body_block():
  <script type="text/javascript">
  function start() {GetSummary();GetFailures();}

  function GetSummary()
  {
    $("#imgProgress1").show();
    $("#divSummaryArea1").load("{{ _root }}/matrix/{{tpl_vars['filter_tag']}}{{! query_string() }} #divMatrix",
                               function(responseText, statusText, xhr) {
                                   $("#imgProgress1").hide();
                                   if(statusText == "error")
                                     alert("Unable to retrieve tests result data: " +
                                           xhr.status + " - " + xhr.statusText);
                               });
  }
  function GetFailures()
  {
    $("#imgProgress2").show();
    $("#divSummaryArea2").load("{{ _root }}/summary/{{tpl_vars['filter_tag']}}{{! query_string() }} #divFailures",
                               function(responseText, statusText, xhr) {
                                   $("#imgProgress2").hide();
                                   if(statusText == "error")
                                     alert("Unable to retrieve recent failure data: " +
                                           xhr.status + " - " + xhr.statusText);
                               });
  }
  </script>

  %# --------------------------------------------------------------------------
  %# Releases switcher
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='')

  %# --------------------------------------------------------------------------
  %# Build vs Platform Summary
  <div id="divSummaryArea1">Loading summary...</div>
  <img src="{{ _root }}/static/loading.gif" style="display: none;" id="imgProgress1" />
  <hr>
  %# --------------------------------------------------------------------------
  %# Recent Failure Summary
  <div id="divSummaryArea2">Loading recent failures...</div>
  <img src="{{ _root }}/static/loading.gif" style="display: none;" id="imgProgress2" />
%end

%rebase('master.tpl', title='Test Dashboard', query_string=query_string, body_block=body_block)
