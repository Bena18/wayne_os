# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""DB backend for querying AutoTest results DB"""

import logging, re, urllib

import db_interface
import named_filters
import pivot_table
import table_data

from src import settings
from src import whining_errors
from src import wmutils


BUILDBOT_URL = '%(buildbot_root)s/builders/%(builder_name)s/builds/%(number)s'


# Managing DB connections
#
# CloudSQL recommends opening a new connection for each HTTP request.
# See CloudSQL FAQ - "How should I manage connections?"
# https://developers.google.com/cloud-sql/faq#connections
# Handling two requests using one connection results in
# DatabaseError: 0: Connection is already in use
#
# For reusing a single connection Outside GAE get_db_reuse() might be used
# but it has to be extended to reopen in some cases.
# Checking to see if db is open is not a good way, the right thing is to
# reconnect if a query fails with some specific exceptions.


class WhiningModel(db_interface.DBInterface):
    """The DB interface for the dashboard.

    This class is the main entry point of the DB backend. Most views of the
    dashboard have a dedicated function here that returns a dictionary
    with all or most of the data needed for that view.

    Main Complex Views:
        Summary matrix view - get_matrix()
        Failures view - get_failures()
        Platform summary view - get_platform_summary()
    """
    def __init__(self, settings, request):
        super(WhiningModel, self).__init__(settings)
        self._request = request

        # Initialize some caching variables.
        self._tot_release_cached = None
        self._releases_cached = None

        # Check for unexpected query parameters.
        qdict = self._request.query
        unexpected = [k for k in qdict if k not in named_filters.KNOWN_QVARS]
        if unexpected:
            msg = ('Unexpected query parameters: %s. '
                   'Valid query parameters include: %s.'
                   % (', '.join(sorted(unexpected)),
                      ', '.join(sorted(named_filters.KNOWN_QVARS))))
            raise whining_errors.WhiningInputError(msg)

        # Validate the values provided in query string. Because our inputs come
        # from a rater limited alphabet, we have the luxury of of blocking
        # anything containing chars outside that alphabet.
        allowed_re = re.compile(r'^[\-\w\*\.\^\$\[\]\|\?\,]*$')
        # Some constructs common in SQL injection attacks.
        bad_words = ['--', '||', '/*', '*/']
        for v in qdict.values():
            if not allowed_re.match(v) or any(word in v for word in bad_words):
                msg = ('The following query string parameter looks fishy and '
                       'was blocked: %s')
                raise whining_errors.WhiningInputError(msg % v)

    def get_qry(self):
        """Return a db_interface Qry object with common setup."""
        return db_interface.Qry()

    def query_string(self, add={}, remove=[], list_qps=False):
        """
        Centralize handling of query string in for later improvement.

        TODO (kamrik): once we stabilize more with the pattern, write a
        a lengthy description here about how this function is passed all the
        way to the template and called there.

        May need to do parameter checking.

        Reconstruct the query string from request.query dictionary, while
        adding or removing some query parameters. To be used inside templates

        Based on django query_string template tag.

        @param add: items to be added to the query string.
        @param remove: list of keys to remove from the string.
        @param list_qps: convenience helper to return a list of query params.

        """
        qdict = dict(self._request.query)
        if remove is None:
            remove = []
        for k in remove:
            if k in qdict:
                del qdict[k]

        qdict.update(add)

        if list_qps:
            return [(k, v) for k, v in qdict.iteritems()]

        qs = urllib.urlencode(qdict)
        if qs:
            return '?%s' % qs
        else:
            return ''

    def make_href(self, view='', filter_tag=None, remove=[], **qps):
        """
        Construct a URL for internal cross linking.

        The URL looks like this:
            /view/filter_tag?param1=value1A,value1B&param2=value2
        Query string parameters are carried over from the current HTTP request.

        @param view: Name of WMatrix view, the first part of resulting URL.
        @param filter_tag: Filter name. Second part of the URL, omitted if None.
        @param remove: List of parameters to remove from query string.
        @param **qps: Named parameters to add to the query string.

        """
        views_w_filter = ['', 'failures', 'jobs', 'matrix', 'platform',
                          'releases', 'suitestats', 'summary', 'testhealth',
                          'testrun']

        qs = self.query_string(add=qps, remove=remove)
        url = settings.settings.relative_root + '/' + view
        if view in views_w_filter and filter_tag:
            url += '/' + filter_tag
        url += qs
        return url

    def assemble_filters(self, name, platform=None, release=None):
        """Prepare a filter dictionary.

        This function will be called first by many URL handlers to convert
        all the parameters  and query string variables into a "filter"
        dictionary that is later used to query for the data.

        Args:
            name: name of the named filter to start with.
            request: request object (holds query parameters).
            platform: if not None, overrides the platforms specified in the
                named filter and request.
            release: if not None, overrides the releases specified in the
                named filter and request.
        """

        # Override with parameters given in the query string
        # Start with a copy of the named filter
        NAMED_FILTERS = named_filters.NAMED_FILTERS
        if not name in NAMED_FILTERS:
            msg = 'Unknown named filter: %s' % name
            raise whining_errors.WhiningFilterError(msg)
        filters = NAMED_FILTERS[name].copy()
        for k in named_filters.KNOWN_QVARS:
            # TODO (kamrik): write normal conversion and error reporting for QS.
            if k in self._request.query:
                if k in ['days_back']:
                    v = wmutils.string2int(self._request.query[k])
                elif k in ['test_ids']:
                    ids = self._request.query[k].split(',')
                    v = [wmutils.string2int(x) for x in ids]
                elif k in ['hide_missing', 'hide_experimental', 'show_faft_view']:
                    v = wmutils.string2bool(self._request.query[k])
                else:
                    v = self._request.query[k].split(',')
                filters[k] = v

        if 'test_ids' in filters:
            return {'test_ids': filters['test_ids']}

        # Additional overrides if specified
        if platform:
            filters['platforms'] = [platform]
        if release:
            filters['releases'] = [release]
        # If builds are specified, remove min_build and add relevant releases
        # for better queries.
        builds = filters.get('builds')
        if builds:
            filters.pop('days_back', None)
            rels = [b.split('-')[0].strip('Rr') for b in builds]
            rels = sorted(set(wmutils.string2int(r) for r in rels))
            filters['releases'] = rels
            logging.debug('assemble_filters: filters = : %s', filters)
            return filters

        # With no builds given, replace days_back with corresponding min_build.
        days_back = filters.pop('days_back', None)
        if not days_back:
            days_back = settings.settings.default_days_back
        # Some views need to know what value of days_back was used.
        filters['days_back_used'] = days_back
        min_build = self.get_min_build(days_back)
        if not min_build:
            msg = 'Looks like the DB was not updated for at least %d days'
            raise whining_errors.WhiningError(msg % days_back)
        filters['min_build'] = min_build

        # Convert special values like 'tot' and 'all' in releases.
        if 'releases' in filters:
            all_releases = self.get_releases()
            releases = filters['releases']
            if 'all' in releases:
                releases = all_releases
            elif 'tot' in releases:
                s = set(releases)
                s.discard('tot')
                s.add(self.get_tot_release())
                releases = s
            # By this time all releases should be numeric, convert to int
            try:
                releases = set(int(r) for r in releases)
            except ValueError as e:
                msg = 'Failed to convert release number to integer: %s'
                raise whining_errors.WhiningError(msg % e.message)
            releases.intersection_update(all_releases)
            # If we ended up with an empty set of releases, it means the there
            # is no data for requested release(s) within days_back.
            if not releases:
                msg = ('No data found, try widening your filtering criteria by '
                       'setting a larger value for days_back to include older '
                       'data, e.g. &days_back=14'
                       )
                raise whining_errors.WhiningError(msg)
            filters['releases'] = sorted(releases)

        logging.debug('assemble_filters: filters = : %s', filters)
        return filters

    def ensure_one_test_id(self, filters):
        """Ensure there is exactly one test_id in the filter."""
        if not filters.get('test_ids'):
            msg = ('One test_id must be provided for this view.\n'
                   'E.g: ?test_ids=1')
            raise whining_errors.WhiningInputError(msg)
        return int(filters.get('test_ids')[0])

    def ensure_one_release(self, filters):
        """Ensure there is exactly one release in the filter.

        If there is more than one, take the first listed. If there is none,
        take TOT.
        """
        if filters.get('releases'):
            filters['releases'] = [filters['releases'][0]]
        elif not 'builds' in filters:
            filters['releases'] = [self.get_tot_release()]

    def ensure_one_platform(self, filters):
        """
        Ensure there is exactly one platform listed in the filter.

        @raises WhiningInputError if there is no platform given. If there is
                more than one, take the first one listed.

        """
        if filters.get('platforms'):
            filters['platforms'] = [filters['platforms'][0]]
        else:
            msg = ('A platform name must be provided for this view.\n'
                   'E.g: ?platforms=alex')
            raise whining_errors.WhiningInputError(msg)

    def ensure_one_qp(self, qp, filters):
        """
        Ensure that query param is in the filter and contains exactly one value.

        @returns value of the query parameter.
        @raises WhiningInputError: If there are no or multiple values given.

        """
        examples = {
            'modems': '?modems=gobi2k_verizon',
            'platforms': '?platforms=alex',
        }
        values = filters.get(qp)
        if values and len(values) == 1:
            return values[0]

        # If we are here, there is something wrong with the qp, display error.
        msg = ('Exactly one value for "%s" must be provided for this view.\n'
               'E.g: %s')
        msg = msg % (qp, examples[qp])
        raise whining_errors.WhiningInputError(msg)

    def ensure_at_least_one_test(self, filters):
        """Ensure there is at least one test_name listed in the filter.

        Raises WhiningInputError if there is no test_name given.
        """
        if not filters.get('tests'):
            msg = ('A test name must be provided for this view.\n'
                   'E.g: ?tests=power_UiResume')
            raise whining_errors.WhiningInputError(msg)

    def get_tot_release(self):
        """Find the highest release number mentioned in DB (like R25)."""
        if self._tot_release_cached is not None:
            return self._tot_release_cached

        qry = 'SELECT max(release_number) FROM known_tests'
        tot = self.run_query_scalar(qry)
        tot = int(tot)
        self._tot_release_cached = tot
        return tot

    def get_builds(self, hyperlinks=False):
        """Get a list of all builds ever mentioned in the DB."""

        qry = 'SELECT build FROM builds ORDER BY build_idx DESC'
        builds = self.run_query_lst(qry)
        builds.sort(cmp=wmutils.cmp_builds)

        if not hyperlinks:
            return builds

        processed_builds = []

        url = settings.settings.relative_root + '/bvt?%s=%s'
        for b in builds:
            r = b.split('-')[0][1:]
            b = (wmutils.html_hyperlink(url % ('releases', r), r),
                 wmutils.html_hyperlink(url % ('builds', b), b))
            processed_builds.append(b)
        return processed_builds

    def get_min_build(self, days_back):
        """Get earliest build mentioned later than days_back days ago."""
        qry = ['SELECT min(build_idx)',
               'FROM builds',
               'WHERE first_seen_time > adddate(utc_timestamp(), -%d)',
               ]
        qry = '\n'.join(qry) % days_back
        min_build = self.run_query_scalar(qry)
        return min_build

    def get_releases(self, hyperlinks=False):
        """Get a list of all releases ever mentioned in the DB."""
        if self._releases_cached is not None:
            releases = self._releases_cached
        else:
            qry = ['SELECT distinct release_number',
                   'FROM known_tests',
                   'ORDER BY release_number']
            releases = self.run_query_lst(' '.join(qry))
            releases = [int(r) for r in releases if r]
            self._releases_cached = releases
            self._tot_release_cached = releases[-1] if len(releases) else None
        if hyperlinks:
            url = settings.settings.relative_root + '/bvt?releases=%s'
            releases = [wmutils.html_hyperlink(url % r, r) for r in releases]
        return releases

    def _issue_url(self, row):
        """Helper function to create a pre-populated Issue.

        Args:
            row: expected to have fields: 'test_name', 'status', 'reason',
                 'build', 'suite'.

        Returns:
            String of the html hyperlink.
        """
        summary = 'Test problem: %s - %s - %s'
        data = {'build': row['build'],
                'newline': '%0A',
                'reason': row['reason'],
                'ref': self._request.url,
                'status': row['status'],
                'suite': row['suite'],
                'summary': summary % (row['test_name'], row['status'],
                                      row['reason'][:40]),
                'test': row['test_name']}
        # Include default summary in the bug body because it disappears if
        # user clicks on summary textbox.
        return wmutils.issue_url(summary=data['summary'], description=(
            'TEST: %(test)s%(newline)s'
            'SUITE: %(suite)s%(newline)s'
            'CHROMEOS BUILD: %(build)s%(newline)s'
            'STATUS: %(status)s%(newline)s'
            'REASON: %(reason)s%(newline)s%(newline)s'
            'SUMMARY: %(summary)s%(newline)s%(newline)s'
            'REF: %(ref)s%(newline)s') % data)

    def _research_url(self, test_name):
        """Helper function to research pre-existing, related issues.

        Example:
            http://code.google.com/p/chromium-os/issues/list?&q=power_UiResume

        Args:
            test_name: used to search for previous failures.

        Returns:
            String of the html hyperlink.
        """
        return '%s/list?q=%s' % (wmutils.BUG_URL_BASE, test_name)

    def get_comments(self):
        """Get a list of triage comments.

        Builds 3 tables:
        -General test information.
        -Previous comments on this test.
        -Comments on other, recent instances of this test.
        """
        test_td = table_data.TableData()
        test_td.sort_cols = False

        idx_fields = ['test_name', 'suite', 'build', 'platform', 'status']
        other_fields = ['release_number', 'reason']
        test_fields = idx_fields + other_fields
        qry_fields = [f + '_idx' for f in idx_fields] + other_fields
        filters = self.assemble_filters('unfiltered')
        qry = self.get_qry()
        qry.fields = qry_fields
        test_id = self.ensure_one_test_id(filters)
        qry.conditions = ['test_idx = %s' % test_id]
        test_name = issue_link = research_link = None
        with self.run_query(qry) as cursor:
            for d in cursor:
                test_name = d['test_name']
                research_link = self._research_url(test_name)
                issue_link = self._issue_url(d)
                url = ('/failures/unfiltered?'
                       'tests=%s&builds=%s&suites=%s&platforms=%s')
                url %= (d['test_name'], d['build'], d['suite'], d['platform'])
                d['test_name'] = wmutils.html_hyperlink(url, d['test_name'])
                for f in test_fields:
                    if d[f] is not None:
                        test_td.add(test_id, f, d[f])

        comment_td = table_data.TableData()
        comment_td.sort_cols = False
        comment_td.row_sort_fn = lambda l: sorted(l, reverse=True)
        related_td = table_data.TableData()
        related_td.sort_cols = False
        related_td.row_sort_fn = lambda l: sorted(l, reverse=True)

        comment_fields = ['created_time', 'comment']
        qry = self.get_qry()
        qry.fields = qry_fields + comment_fields + ['test_idx', 'comment_idx']
        qry.table = 'triage_comments'
        where = ('test_name_idx = '
                 '(SELECT test_name_idx FROM test_names WHERE test_name="%s")'
                 ) % test_name
        qry.conditions = [where]
        with self.run_query(qry) as cursor:
            for d in cursor:
                d['created_time'] = d['created_time'].strftime(
                    '%a %b %d, %H:%M')
                d['test_name'] = wmutils.html_hyperlink(
                    '/comments?test_ids=%s' % d['test_idx'], test_name)
                for f in (test_fields + comment_fields):
                    if d[f] is not None:
                        if d['test_idx'] == test_id:
                            if f in comment_fields:
                                comment_td.add(d['comment_idx'], f, d[f])
                        else:
                            related_td.add(d['comment_idx'], f, d[f])

        return {'test_id': test_id, 'test_td': test_td,
                'comment_td': comment_td, 'related_td': related_td,
                'issue_link': issue_link, 'research_link': research_link}

    def get_summary(self, filter_tag):
        """Get a list of recent failures aggregated by test name."""
        filters = self.assemble_filters(filter_tag)
        idx_fields = ['test_name', 'suite', 'build', 'platform', 'status']
        other_fields = ['test_finished_time', 'reason']
        fields = idx_fields + other_fields
        qry = self._query_test_list(filters)
        qry.fields = [f + '_idx' for f in idx_fields] + other_fields

        td = table_data.TableData(
            'test_name', cell_default=table_data.CellDataSet)
        td.sort_rows = False
        # build is formatted separately
        with self.run_query(qry) as cursor:
            for d in cursor:
                for f in fields[1:]:
                    td.add(d['test_name'], f, d[f])
        return td

    def get_platform_summary(self, filter_tag):
        """Data for platform view."""
        timer = wmutils.Timer()

        filters = self.assemble_filters(filter_tag)
        self.ensure_one_release(filters)
        if filter_tag == '3g':
            self.ensure_one_qp('modems', filters)
        else:
            self.ensure_one_qp('platforms', filters)
        td = table_data.SortedBuildTableData(
            'Test Name', cell_default=table_data.GoodFailOtherCellData)
        experimental_test_names = set()

        for tbl in ['bad_tests', 'good_tests']:
            qry = self._query_test_list(filters, table=tbl)
            timer.start('platform_summary_qry_' + tbl)
            with self.run_query(qry) as cursor:
                timer.stop('platform_summary_qry_' + tbl)
                timer.start('platform_summary_loop_' + tbl)
                for d in cursor:
                    test_name = d['test_name']
                    if filters.get('show_faft_view'):
                        td.add(row=d['build'], col=test_name, data=d['status'],
                               test_idx=d['test_idx'], test_name=test_name,
                               fw_rw_version=d['fw_rw_version'],
                               fw_ro_version=d['fw_ro_version'],
                               test_version=d['test_version'])
                    else:
                        td.add(row=d['build'], col=test_name, data=d['status'],
                               test_idx=d['test_idx'], test_name=test_name)
                    if d['is_experimental']:
                        experimental_test_names.add(d['test_name'])
                timer.stop('platform_summary_loop_' + tbl)

        return {'table_data': td,
                'experimental_test_names': experimental_test_names,
                'release': filters.get('releases', [''])[0],
                'timings': timer.timings,
                'timings_str': str(timer),
                'show_faft_view': filters.get('show_faft_view')}

    def get_platforms(self, hyperlinks=False):
        """Get a list of all platforms ever mentioned in the DB."""
        qry = 'SELECT platform FROM platforms ORDER BY platform'
        platforms = self.run_query_lst(qry)
        if hyperlinks:
            new_platforms = []
            url = settings.settings.relative_root + '/%s?platforms=%s'
            for p in platforms:
                bvt_link = wmutils.html_hyperlink(url % ('bvt', p), 'bvt')
                platform_link = wmutils.html_hyperlink(
                    url % ('platform/bvt', p), 'bvt')
                new_platforms.append((p, bvt_link, platform_link))
            platforms = new_platforms
        return platforms

    def get_suites(self, hyperlinks=False):
        """Get a list of all suites ever mentioned in the DB."""
        qry = 'SELECT suite FROM suites ORDER BY suite'
        suites = self.run_query_lst(qry)
        if hyperlinks:
            suites = [wmutils.html_hyperlink(
                '%s/unfiltered?releases=tot&suites=%s' %
                (settings.settings.relative_root, s), s) for s in suites]
        return suites

    def get_suite_stats(self, filter_tag):
        """Get a list of suite statistics.

        The most useful stat is suite duration.
        Suites are bucketed by release, platform, suite.
        """
        def _to_minutes(s):
            """Convert a float of seconds to minutes (and handle None)."""
            return round(s / 60.0, 1) if s else 0.0
        def _to_1_decimal(f):
            """Round to 1 place after the decimal (and handle None)."""
            return round(f, 1) if f else 0.0
        # suite_stats does not offer 'builds' so avoid min_build.
        filters = self.assemble_filters(filter_tag)
        # Need to pop min_build to allow release hyperlinking.
        filters.pop('min_build')
        link_fields = ['suite', 'platform', 'release_number']
        fields = ['avg_duration', 'std_duration', 'runs_total',
                  'days_observed', 'runs_per_week']
        qry = self.get_qry()
        qry.fields = ['suite_idx', 'platform_idx', 'release_number'] + fields
        qry.table = 'suite_stats'
        qry.filters = filters
        qry.order_by = 'suite, platform, release_number'

        td = table_data.TableData()
        td.sort_cols = False
        td.sort_rows = False
        qp_lookup = qry.qp_from_db_fields()

        timer = wmutils.Timer()
        timer.start('qry_suite_stats')
        with self.run_query(qry) as cursor:
            for d in cursor:
                row_key_fields = ['suite', 'platform', 'release_number']
                row_key = '/'.join(str(d[f]) for f in row_key_fields)
                # Links on common filter fields.
                for f in link_fields:
                    url = '%s/suitestats%s' % (settings.settings.relative_root,
                        self.query_string(add={qp_lookup[f]: d[f]}))
                    td.add(row_key, f, wmutils.html_hyperlink(url, d[f]))
                # Clarify duration titles, convert to minutes and round.
                td.add(row_key, 'Avg (min)', _to_minutes(d['avg_duration']))
                td.add(row_key, 'Std Dev (min)', _to_minutes(d['std_duration']))
                # Regular stats with custom rounding.
                td.add(row_key, 'runs_total', d['runs_total'])
                td.add(row_key, 'days_observed', d['days_observed'])
                td.add(row_key, 'runs_per_week',
                       _to_1_decimal(d['runs_per_week']))
        timer.stop('qry_suite_stats')

        return {'table_data': td}

    def get_modems(self):
        """A special modems view for 3g tests.

        This view is similar to matrix view but the columns are modem-carrier
        combinations instead of platforms.

        The modem and carrier are added as a tag to the end of test names.
        Example test names:
          network_3GSmokeTest.gobi3k_verizon
          network_3GSmokeTest.y3300_tmobile
          network_3GStressEnable.gobi3k
          network_3GModemPresent.y3300
          network_3GModemPresent.gobi2k
          network_3GModemControl.no-autoconnect-gobi2k_tmobile
        """
        filters = self.assemble_filters('3g')

        td = table_data.SortedBuildTableData(
            'Modem-carrier', cell_default=table_data.GoodFailOtherCellData)
        td.sort_cols = True
        td.sort_rows = False

        td.init_custom_row_header('Start Time',
                                  table_data.CustomHeaderCollector_Min())

        timer = wmutils.Timer()
        for table in ['bad_tests', 'good_tests']:
            timer.start('qry_modems_%s' % table)
            qry = self._query_test_list(filters, table=table)
            qry.order_by = 'build_idx DESC'
            with self.run_query(qry) as cursor:
                for d in cursor:
                    buildname = d['build']
                    test_name = d['test_name']
                    mc = test_name.replace('-', '.').split('.')[-1]
                    # mc is either like 'gobi2k' or 'gobi2k_tmobile'
                    td.add(row=buildname, col=mc, data=d['status'],
                           test_name=test_name)
                    td.add_custom_row_header('Start Time', buildname,
                                             d['test_started_time'])
            timer.stop('qry_modems_%s' % table)

        return {'table_data': td}

    def _get_faft_versions(self, query, filters):
        """Update query to include faft related fields if filters has
        show_faft_view set to True.

        Args:
            query: A query object.
            filters: assembled filter object (usually from assemble_filters().
        """
        ver_substring = 'SUBSTRING_INDEX(%(version)s, "/", -1) as %(version)s'
        if filters.get('show_faft_view'):
            query.fields.extend(
                    [ver_substring % {'version': 'fw_rw_version'},
                     ver_substring % {'version': 'fw_ro_version'},
                     ver_substring % {'version': 'test_version'}])

    def _query_pass_counts(self, filters):
        """Helper to build query for passed (GOOD) tests.

        Args:
            filters: assembled filter object (usually from assemble_filters().

        Returns:
            A qry object.
        """
        qry = self.get_qry()
        qry.table = 'good_tests'
        qry.fields = ['platform_idx', 'build_idx', 'count(1) as cnt ',
                      'min(test_started_time) as test_started_time']
        self._get_faft_versions(qry, filters)
        qry.filters = filters
        qry.group_by = 'platform_idx, build_idx'
        return qry

    def _query_test_list(self, filters, extra_fields=[], table='bad_tests'):
        """Helper to build query for a list of tests.

        Args:
            filters: assembled filter object (usually from assemble_filters()
            extra_fields: a minority of users of this query need extra columns.
            table: either 'bad_tests' (default) or 'good_tests'.
                TODO (kamrik): Add option to combine both tables with a UNION.

        Returns:
            A Qry object.
        """
        qry = self.get_qry()
        qry.filters = filters
        qry.fields = ['test_idx', 'platform_idx', 'build_idx', 'test_name_idx',
                      'test_started_time', 'is_experimental']
        qry.fields.extend(extra_fields)
        self._get_faft_versions(qry, filters)
        qry.table = table
        if table == 'good_tests':
            qry.fields.append("'GOOD' AS status")
        else:
            qry.fields.append('status_idx')
        return qry

    def get_matrix(self, filter_tag):
        """Data for the matrix view."""
        timer = wmutils.Timer()
        timer.start('call_get_matrix')

        timer.start('call_assemble_filters')
        filters = self.assemble_filters(filter_tag)
        timer.stop('call_assemble_filters')

        pt = pivot_table.PivotTable()
        params = {'ngood': 0,
                  'chromever': '?',
                  'simplified_result': None}

        if filters.get('show_faft_view'):
            params['fw_rw_version'] = '?'
            params['fw_ro_version'] = '?'
            params['test_version'] = '?'
            # Force hide_missing to be True for FAFT view.
            filters['hide_missing'] = True

        cell_factory = pivot_table.make_cell_factory(
                pivot_table.MultiListCell, **params)
        pt.add_grouping(('build', 'platform'), cell_factory, 'bp')
        pt.add_grouping('build', cell_factory)

        # clean() is a function that copies a subset of values from a dict.
        subfields = ['build', 'platform', 'test_started_time', 'expected_time']
        if filters.get('show_faft_view'):
            subfields.extend(['fw_rw_version', 'fw_ro_version', 'test_version'])
        clean = wmutils.make_dict_cleaner(subfields)

        # To optimize query run time, avoid retrieving test_name and
        # status for successful (GOOD) test results. For large summaries,
        # there may be many, long test_names (e.g. Browsertests contains many
        # long-named tests). The test names are not used in the reports for
        # passing tests so this is wasted bandwidth.
        #
        # First grab the failing results (with the test_names and status).
        timer.start('qry_failures')
        qry = self._query_test_list(filters)
        with self.run_query(qry) as cursor:
            for row in cursor:
                d = clean(row)
                test_name = row['test_name']
                if row['is_experimental']:
                    test_name = 'EXP:' + test_name
                if row['status'] == 'FAIL':
                    d['failed_tests'] = test_name
                else:
                    d['problem_tests'] = test_name
                pt.add_dict(d)
        timer.stop('qry_failures')

        timer.start('qry_success')
        qry = self._query_pass_counts(filters)
        with self.run_query(qry) as cursor:
            for row in cursor:
                pt.add_dict(clean(row, ngood='cnt'))
        timer.stop('qry_success')

        # Missing suites
        if not filters.get('hide_missing'):
            timer.start('qry_missing_suites')
            qry = self.get_qry()
            # missing_suites table has no test_name field
            qry.filters = filters.copy()
            if 'tests' in qry.filters:
                del qry.filters['tests']
            qry.table = 'missing_suites'
            qry.fields = ['platform_idx', 'build_idx', 'suite_idx',
                          'expected_time']
            with self.run_query(qry) as cursor:
                for row in cursor:
                    pt.add_dict(clean(row, missing_suites='suite'))
            timer.stop('qry_missing_suites')

        # Missing tests
        if not filters.get('hide_missing'):
            timer.start('qry_missing_tests')
            qry = self.get_qry()
            qry.filters = filters
            qry.table = 'missing_tests'
            qry.fields = ['platform_idx', 'build_idx', 'test_name_idx']
            with self.run_query(qry) as cursor:
                for row in cursor:
                    pt.add_dict(clean(row, missing_tests='test_name'))
            timer.stop('qry_missing_tests')

        # We only want to display platforms that had some test data.
        platforms = sorted(pt.keylists['platform'])

        # Build info
        timer.start('qry_chrome_versions')
        qry = self.get_qry()
        qry.filters = {
            'releases': filters.get('releases'),
            'builds': filters.get('builds'),
            'min_build': filters.get('min_build'),
            }
        qry.filters
        qry.table = 'images'
        qry.fields = ['build_idx', 'platform_idx', 'chromever', 'number',
                      'simplified_result', 'buildbot_root', 'builder_name']
        with self.run_query(qry) as cursor:
            pt.add_dictlist(cursor)
        timer.stop('qry_chrome_versions')

        # Post-process cells for the row headers (time & chrome version).
        for cell in pt.data['build'].values():
            if 'test_started_time' in cell.lists:
                cell.time = min(cell.lists['test_started_time'])
            elif 'expected_time' in cell.lists:
                cell.time = min(cell.lists['expected_time'])
            else:
                cell.time = None
                cell.strtime = '?'
                continue
            cell.strtime = cell.time.strftime('%a %b %d, %H:%M')

        # Post-process cell for the main 2D part of the matrix.
        # Names of the lists we want to extract from the lists in each cell:
        list_names = ['failed_tests', 'problem_tests', 'missing_tests',
                      'missing_suites']
        for cell in pt.data['bp'].values():
            # Set defaults:
            for lst in list_names:
                cell.__dict__[lst] = cell.lists.get(lst, [])
            cell.nfail = len(cell.failed_tests)
            cell.failed_tests.sort()
            cell.nother = len(cell.problem_tests)
            cell.problem_tests.sort()
            cell.nmissing_suites = len(cell.missing_suites)
            cell.missing_suites.sort()
            cell.nmissing_tests = len(cell.missing_tests)
            cell.missing_tests.sort()
            cell.ntotal = (cell.ngood + cell.nfail + cell.nother +
                           cell.nmissing_tests + cell.nmissing_suites)
            cell.build_status = cell.simplified_result
            cell.bld_href = None
            if hasattr(cell, 'builder_name'):
                cell.bld_href = BUILDBOT_URL % cell.__dict__

        builds = pt.keylists['build']
        builds.sort(key=wmutils.build2sortable, reverse=True)

        timer.stop('call_get_matrix')
        return {'pt': pt,
                'builds': builds,
                'platforms': platforms,
                'timings': timer.timings,
                'timings_str': str(timer)}

    def get_failures(self, filter_tag):
        """The failures view is similar to summary view with less aggregation.

        However, instead of grouping data by test_name and field (build,
        platform, reason, status, suite...), each test is shown separately.
        """
        timer = wmutils.Timer()
        timer.start('assemble_filter')
        filters = self.assemble_filters(filter_tag)
        timer.stop('assemble_filter')

        display_fields = ['status', 'build', 'time', 'platform', 'suite',
                          'test_name', 'auto_bug', 'reason']
        qry_tests = self._query_test_list(filters)
        qry_tests.join = ('LEFT JOIN autobugs USING(test_idx) '
                          'JOIN tko_status ON '
                          '(bad_tests.status_idx = tko_status.status_idx)'
                          )
        qry_tests.fields = """
            bad_tests.suite_idx,
            bad_tests.build_idx,
            bad_tests.platform_idx,
            bad_tests.test_name_idx,
            tko_status.word as status,
            bad_tests.reason AS reason,
            bad_tests.test_finished_time AS time,
            test_idx AS idx,
            CONCAT('TEST_', test_idx) AS row_key,
            autobugs.bug_id AS auto_bug,
            afe_job_id
            """

        qry_mt = self.get_qry()
        qry_mt.filters = filters
        qry_mt.table = 'missing_tests'
        qry_mt.fields = """
            missing_tests.suite_idx,
            missing_tests.build_idx,
            missing_tests.platform_idx,
            missing_tests.test_name_idx,
            'MISSING_TEST' AS status,
            '' AS reason,
            missing_tests.expected_time AS time,
            CONCAT('MT_', missing_test_idx) AS idx,
            CONCAT('MT_', missing_test_idx) AS row_key,
            NULL AS auto_bug,
            0 AS afe_job_id
            """
        qry_ms = self.get_qry()
        qry_ms.filters = filters
        qry_ms.table = 'missing_suites'
        qry_ms.fields = """
            missing_suites.suite_idx,
            missing_suites.build_idx,
            missing_suites.platform_idx,
            '' AS test_name,
            'MISSING_SUITE' AS status,
            '' AS reason,
            missing_suites.expected_time AS time,
            CONCAT('MS_', missing_suite_idx) AS idx,
            CONCAT('MS_', missing_suite_idx) AS row_key,
            NULL AS auto_bug,
            0 AS afe_job_id
            """

        # Combine the 3 queries into one huge UNION with sorting after it.
        # Apart from sorting, hopefully, we gain parallelization of the 3 parts.
        # Note: for the UNION to work, all queries must have identical columns.
        # If tests are specified in the filter, we can't show missing suites,
        # TODO (kamrik): consider using "JOIN known_tests USING (suite)"
        if filters.get('hide_missing') or filter_tag == '3g':
            queries = [qry_tests]
        elif filters.get('tests'):
            queries = [qry_tests, qry_mt]
        else:
            queries = [qry_tests, qry_mt, qry_ms]

        qry_union = '\n\nUNION\n\n'.join(str(q) for q in queries)
        qry_all = '\n'.join([
            'SELECT * FROM (\n%s\n) AS t \n',
            'JOIN suites USING (suite_idx)',
            'JOIN builds USING (build_idx)',
            'JOIN platforms USING (platform_idx)',
            'JOIN test_names USING (test_name_idx)',
            'ORDER BY build_idx DESC, time DESC',
            ])
        qry_all = qry_all % qry_union

        td = table_data.TableData('row_key',
                                  cell_default=table_data.CellDataSet)
        td.sort_cols = False
        td.sort_rows = False

        timer.start('qry_failures_union')
        with self.run_query(qry_all) as cursor:
            for d in cursor:
                for column, value in d.iteritems():
                    td.add(d['row_key'], column, value)
        timer.stop('qry_failures_union')

        return {'table_data': td, 'fields': display_fields}

    def get_tests(self, exclude_suites=[], hyperlinks=False):
        """Get a list of all tests ever mentioned in the DB."""
        qry = self.get_qry()
        qry.days_back = None
        qry.fields = 'distinct test_name_idx'
        qry.translate = ['test_name_idx']
        qry.table = 'known_tests'
        if exclude_suites:
            cond = ('suite_idx NOT IN\n  '
                    '(SELECT suite_idx FROM suites WHERE suite IN ("%s"))'
                    ) % '", "'.join(exclude_suites)
            qry.conditions.append(cond)
        qry.order_by = 'test_name'
        with self.run_query(qry) as cursor:
            tests = [d['test_name'] for d in cursor]
        if hyperlinks:
            url = '%s/unfiltered?hide_missing=True&releases=tot&tests=%s'
            tests = [wmutils.html_hyperlink(url %
                                        (settings.settings.relative_root, t), t)
                     for t in tests]
        return tests

    def get_testrun(self, filter_tag):
        filters = self.assemble_filters(filter_tag)
        # TODO(kamrik): we don't want any days_back here
        test_ids = filters.get('test_ids')
        if not test_ids:
            msg = ('At least one test id must be specified for this view, '
                   'e.g. test_ids=3,7'
                   )
            raise whining_errors.WhiningError(msg)

        str_ids = ', '.join(str(id) for id in test_ids)
        # Note that good_tests/bad_tests left-joining autobugs,
        # good_tests and bad_tests could result in multiple fields of
        # 'test_idx' if we select '*'.
        # cloudsql python api doesn't prepend table alias to the field names.
        # we need to be careful that the resulting fields
        # have unique names.
        qry_tpl = '\n'.join([
            'SELECT {table}.*, suites.*, builds.*, platforms.*, ',
            '    test_names.*, configs.*, tko_machines.*, autobugs.bug_id, ',
            '    tko_status.word as status, ',
            '    {table}.invalidates_test_idx as retry_of, ',
            '    GROUP_CONCAT(IF(t2.test_idx is NOT NULL, ',
            '        t2.test_idx, t3.test_idx) SEPARATOR \',\') as retried_by',
            'FROM {table}',
            'JOIN suites USING (suite_idx)',
            'JOIN builds USING (build_idx)',
            'JOIN platforms USING (platform_idx)',
            'JOIN test_names USING (test_name_idx)',
            'JOIN configs USING (config_idx)',
            'JOIN tko_status USING (status_idx)',
            'JOIN tko_machines USING (machine_idx)',
            'LEFT JOIN autobugs USING (test_idx)',
            'LEFT JOIN good_tests as t2 '
            'ON (t2.invalidates_test_idx={table}.test_idx)',
            'LEFT JOIN bad_tests as t3 '
            'ON (t3.invalidates_test_idx={table}.test_idx)',
            'WHERE {table}.test_idx IN ({str_ids})',
            'GROUP BY {table}.test_idx',
            ])
        test_runs = []
        for tbl in ['bad_tests', 'good_tests']:
            qry = qry_tpl.format(table=tbl, str_ids=str_ids)
            with self.run_query(qry) as cursor:
                test_runs += list(cursor)

        return test_runs


    def get_jobs(self, filter_tag):
        """Show jobs related to a build+platform from platfom view.

        Used to debug odd test counts by reviewing the job names that ran.
        """
        filters = self.assemble_filters(filter_tag)
        td = table_data.TableData()
        td.sort_rows = False
        url = '%s/afe/#tab_id=view_job&object_id=%s'

        qry = self.get_qry()
        qry.filters = filters
        qry.fields = 'job_name, afe_job_id'
        qry.order_by = 'job_name'
        for tbl in ['bad_tests', 'good_tests']:
            qry.table = tbl
            with self.run_query(qry) as cursor:
                for row in cursor:
                    data = wmutils.html_hyperlink(url %
                        (settings.settings.autotest_host, row['afe_job_id']),
                                                  row['job_name'])
                    td.add(row['afe_job_id'], '', data)

        return {'table_data': td}

    def compute_test_status_rates(self, cells, pass_rate=True):
        """
        A helper function to calculate test status rates from counts.

        @param cells: A sequence or iterator with cell objects to work on.
        @param pass_rate: If True, compute pass rate.
                          If False, compute failure rate.

        """
        for cell in cells:
            if not hasattr(cell, 'bad_count'):
                cell.bad_count = 0
            if not hasattr(cell, 'good_count'):
                cell.good_count = 0
            cell.all_count = cell.bad_count + cell.good_count
            desired_count = cell.good_count if pass_rate else cell.bad_count
            cell.rate = 1.0 * desired_count / cell.all_count
            cell.str_rate = wmutils.format_rate(cell.rate)

    def get_suitehealth(self):
        """Health view that shows failure rates per suite per platform."""
        filters = self.assemble_filters('health')
        self.ensure_one_release(filters)
        days_back = filters.get('days_back_used')

        # Set up pivot table for the data
        pt = pivot_table.PivotTable()
        pt.add_grouping(('suite', 'platform'), pivot_table.TakeLastCell, 'sp')
        sum_fields = ['bad_count', 'good_count']
        cell_factory = pivot_table.make_cell_factory(
            pivot_table.SumCell,
            sum_fields=sum_fields,
            )
        pt.add_grouping('suite', cell_factory)
        pt.add_grouping('platform', cell_factory)
        pt.add_grouping(tuple(), cell_factory, 'totals')

        # Execute query
        for status in ['good', 'bad']:
            qry = db_interface.Qry()
            qry.filters = filters
            if status == 'bad':
                # Exclude any test with status TEST_NA
                qry.conditions = ['status_idx <> (SELECT status_idx FROM '
                                  'tko_status WHERE word="TEST_NA")']
            qry.table = '%s_tests' % status
            qry.fields = ['suite_idx', 'platform_idx',
                          'COUNT(1) AS %s_count' % status,
                          ]
            qry.group_by = 'suite_idx, platform_idx'
            with self.run_query(qry) as cursor:
                pt.add_dictlist(cursor)

        # Calculate and string-format failure rates from counts
        self.compute_test_status_rates(pt.all_cells(), pass_rate=False)
        pt.days_back = days_back
        return pt


    def get_testhealth(self, filter_tag):
        """Show failure rates per test per platform."""
        filters = self.assemble_filters(filter_tag)
        self.ensure_one_release(filters)
        days_back = filters.get('days_back_used')

        # Set up pivot table for the data
        pt = pivot_table.PivotTable()
        pt.add_grouping(('test', 'platform'), pivot_table.TakeLastCell, 'tp')
        sum_fields = ['bad_count', 'good_count', 'is_exp']
        cell_factory = pivot_table.make_cell_factory(
            pivot_table.SumCell,
            sum_fields=sum_fields,
            )
        pt.add_grouping('test', cell_factory)
        pt.add_grouping('platform', cell_factory)
        pt.add_grouping(tuple(), cell_factory, 'total')
        pt.days_back = days_back

        # Run queries and populate the pivot table
        for status in ['good', 'bad']:
            qry = db_interface.Qry()
            qry.filters = filters
            if status == 'bad':
                # Exclude any test with status TEST_NA
                qry.conditions = ['status_idx <> (SELECT status_idx FROM '
                                  'tko_status WHERE word="TEST_NA")']
            qry.table = '%s_tests' % status
            qry.fields = ['test_name_idx', 'suite_idx', 'platform_idx',
                          'COUNT(1) AS %s_count' % status,
                          'MIN(is_experimental) AS is_exp'
                          ]
            qry.group_by = 'test_name_idx, suite_idx, platform_idx'

            with self.run_query(qry) as cursor:
                for d in cursor:
                    test = d['suite'] + '/' + d['test_name']
                    d['test'] = test
                    pt.add_dict(d)
        # Calculate and string-format failure rates from counts
        self.compute_test_status_rates(pt.all_cells(), pass_rate=False)
        return pt


    def get_reasons(self):
        """List failure reasons for a given test with frequencies."""

        filters = self.assemble_filters('health')
        test_name = self.ensure_one_qp('tests', filters)

        # Set up pivot table for the data
        pt = pivot_table.PivotTable()
        pt.add_grouping(('rsn', 'platform'), pivot_table.TakeLastCell, 'rp')
        pt.add_grouping('rsn', pivot_table.ListCell)
        pt.add_grouping('platform', pivot_table.ListCell)
        pt.days_back = filters.get('days_back_used')
        pt.test_name = test_name

        # Execute query
        qry = db_interface.Qry()
        qry.filters = filters
        qry.fields = ['test_name_idx', 'platform_idx', 'reason']
        grand_total = 0
        with self.run_query(qry) as cursor:
            for d in cursor:
                grand_total += 1
                reason = d['reason']
                # Remove all numbers in order to group similar reason strings
                d['rsn'] = wmutils.normalize_reason(reason)
                pt.add_dict(d)

        for cell in pt.all_cells():
            cell.count = cell._add_count

        for cell in pt.data['rsn'].values():
            cell.percent = 1.0 * cell.count / grand_total
            cell.str_percent = wmutils.format_rate(cell.percent)
            cell.reasons = set(d['reason'] for d in cell.lst)

        for cell in pt.data['rp'].values():
            pcount = pt.data['platform'][cell.platform].count
            cell.percent = 1.0 * cell._add_count / pcount
            cell.str_percent = wmutils.format_rate(cell.percent)

        return pt


    def compute_avg_retry_count(self, cells):
        """
        A helper function to calculate average retry count.

        @param cells: A sequence or iterator with cell objects to work on.

        """
        for cell in cells:
            if not hasattr(cell, 'bad_count'):
                cell.bad_count = 0
            if not hasattr(cell, 'good_count'):
                cell.good_count = 0
            if not hasattr(cell, 'bad_retry_count'):
                cell.bad_retry_count = 0
            if not hasattr(cell, 'good_retry_count'):
                cell.good_retry_count = 0
            cell.all_count = cell.bad_count + cell.good_count
            cell.avg_retry_count = float(
                    cell.good_retry_count +
                    cell.bad_retry_count) / cell.all_count
            cell.str_avg_retry_count = '{:5.1f}'.format(cell.avg_retry_count)


    def get_retry_teststats(self, filter_tag):
        """Show test retry stats per platform."""
        filters = self.assemble_filters(filter_tag)
        # Invalid tests are failing tests that have been retried
        # Include such tests for retry stats.
        filters['include_invalid'] = True
        self.ensure_one_release(filters)
        days_back = filters.get('days_back_used')

        # Set up pivot table for the data
        pt = pivot_table.PivotTable()
        pt.add_grouping(('test', 'platform'), pivot_table.TakeLastCell, 'tp')
        sum_fields = ['bad_count', 'good_count', 'is_exp', 'retry_count']
        cell_factory = pivot_table.make_cell_factory(
            pivot_table.SumCell,
            sum_fields=sum_fields,
            )
        pt.add_grouping('test', cell_factory)
        pt.add_grouping('platform', cell_factory)
        pt.add_grouping(tuple(), cell_factory, 'total')
        pt.days_back = days_back

        results = {}
        # Run queries and populate the pivot table
        cond_count = ('{table}.invalid=0 '
                      'AND {table}.invalidates_test_idx IS NOT NULL')
        cond_count_retries = '{table}.invalidates_test_idx IS NOT NULL'
        config = [('good_tests', 'good_count', cond_count),
                  ('bad_tests', 'bad_count', cond_count),
                  ('good_tests', 'good_retry_count', cond_count_retries),
                  ('bad_tests', 'bad_retry_count', cond_count_retries)]

        results = {}
        for table, count_column, condition in config:
            qry = db_interface.Qry()
            qry.table = table
            qry.filters = filters
            qry.conditions = [condition.format(table=table)]
            joins= ' '.join([
                'JOIN bad_tests as t2 ON '
                '({table}.retry_orig_test_idx=t2.test_idx)',
                'JOIN suites s ON (s.suite_idx = {table}.suite_idx)',
                'JOIN platforms p ON (p.platform_idx = {table}.platform_idx)',
                'JOIN test_names n ON (n.test_name_idx = {table}.test_name_idx)'
                ])
            qry.join = joins.format(table=table)
            qry.fields = [
                    '%s.test_name_idx' % table,
                    '%s.suite_idx' % table,
                    '%s.platform_idx' % table,
                    'test_name', 'suite', 'platform',
                    'COUNT(1) AS %s' % count_column,
                    'MIN(%s.is_experimental) AS is_exp' % table,
                    'GROUP_CONCAT(%s.test_idx SEPARATOR \',\') AS test_list' % table]
            qry.group_by = ('{table}.test_name_idx, {table}.suite_idx, '
                            '{table}.platform_idx').format(table=table)

            with self.run_query(qry) as cursor:
                for d in cursor:
                    test = d['suite'] + '/' + d['test_name']
                    d['test'] = test
                    key = (test, d['platform'])
                    if key in results:
                        results[key][count_column] = d[count_column]
                        if count_column in ['good_count', 'bad_count']:
                            results[key]['test_list'] += ',' + d['test_list']
                    else:
                        results[key] = d
        for k, d in results.iteritems():
            if d.get('good_count') or d.get('bad_count'):
                pt.add_dict(d)
        # Calculate and string-format retry pass rates from counts
        self.compute_test_status_rates(pt.all_cells(), pass_rate=True)
        # Calculate ans string-format average retry counts
        self.compute_avg_retry_count(pt.all_cells())
        return pt

    def get_retry_hoststats(self, filter_tag):
        """Show retry hosts stats per platform."""
        min_count = 5
        min_rate = 0.2

        # Only support days_back filter
        # days_back is translated to min_build, so we will only
        # append min_build constraint to the sql later
        filters = self.assemble_filters(filter_tag)
        days_back = filters.get('days_back_used')
        min_build = filters.get('min_build') if days_back else None

        pt = pivot_table.PivotTable()
        pt.days_back = days_back
        pt.add_grouping(('platform', 'host'), pivot_table.ListCell, 'ph')
        pt.add_grouping('platform', pivot_table.ListCell)
        pt.add_grouping('host', pivot_table.ListCell)

        select_tpl = '\n'.join([
                'SELECT t2.machine_idx, t2.platform_idx, %d AS is_good',
                'FROM %s AS t1',
                'JOIN bad_tests AS t2 ON (t2.test_idx = t1.retry_orig_test_idx',
                'AND t2.machine_idx != t1.machine_idx',
                'AND t1.build_idx >= %d' % min_build if min_build else '',
                ')',
                'WHERE t1.invalidates_test_idx IS NOT NULL AND t1.invalid = 0',
                ])

        # for each host get chains that end up failed/passed
        select_bad = select_tpl % (0, 'bad_tests')
        select_good = select_tpl % (1, 'good_tests')

        # aggregate by platforma and host
        select_union = '\n'.join([
                'SELECT u.platform_idx, u.machine_idx, COUNT(1) AS all_count,',
                'SUM(u.is_good) AS good_count',
                'FROM ((%s) UNION ALL (%s)) AS u',
                'GROUP BY u.platform_idx, u.machine_idx',
                'ORDER BY all_count, u.platform_idx, u.machine_idx',
                ]) % (select_bad, select_good)

        # get retry pass rate and filter and order
        qry = '\n'.join([
                'SELECT p.platform, m.hostname, s.all_count, s.good_count,',
                's.good_count/s.all_count AS rate',
                'FROM (%s) AS s',
                'JOIN platforms AS p USING (platform_idx)',
                'JOIN tko_machines AS m USING (machine_idx)',
                'WHERE s.all_count >= %d AND s.good_count/s.all_count >= %f',
                'ORDER BY s.platform_idx, rate DESC',
                ]) % (select_union, min_count, min_rate)

        test_runs = []
        with self.run_query(qry) as cursor:
            for d in cursor:
                d['host'] = 'host'
                pt.add_dict(d)

        return pt

    def get_retry_hostinfo(self, filter_tag):
        """Show top flaky reason per host."""
        top_limit = 10
        filters = self.assemble_filters(filter_tag)
        if not filters.get('hostnames'):
            return None
        days_back = filters.get('days_back_used')
        min_build = filters.get('min_build') if days_back else None

        hns = ['\'%s\'' % s for s in filters.get('hostnames')]
        hostnames = ','.join(hns)

        # Get all machines given hostnames
        qry = db_interface.Qry()
        qry.table = 'tko_machines'
        qry.fields = ['machine_idx', 'hostname']
        qry.conditions = ['hostname IN (%s)' % hostnames]
        with self.run_query(qry) as cursor:
            machines = cursor.fetchall()

        # Find retry chains which start with bad tests and end with good tests.
        # Get the top |top_limit| reasons of the heads (original tests)
        # of these chains.
        results = {}
        for machine in machines:
            machine_idx = machine['machine_idx']
            hostname = machine['hostname']
            result = []
            qry = db_interface.Qry()
            qry.table = 'bad_tests'
            qry.min_build = min_build
            qry.conditions = [
                    'bad_tests.invalidates_test_idx IS NULL',
                    'bad_tests.invalid = 1',
                    'bad_tests.machine_idx = %s' % machine_idx,
                    ]
            qry.join = ' '.join([
                    'JOIN good_tests ON',
                    '(good_tests.retry_orig_test_idx = bad_tests.test_idx',
                    'AND good_tests.invalid = 0',
                    'AND good_tests.invalidates_test_idx IS NOT NULL',
                    'AND good_tests.machine_idx != bad_tests.machine_idx)',
                    ])
            qry.group_by = 'bad_tests.reason'
            qry.order_by = 'count DESC'
            qry.limit = top_limit
            qry.fields = [
                    'COUNT(1) AS count',
                    'bad_tests.reason AS reason',
                    'GROUP_CONCAT(bad_tests.test_idx SEPARATOR \',\') AS test_ids',
                    ]
            with self.run_query(qry) as cursor:
                for d in cursor:
                    result.append(d)

            results[(machine_idx, hostname)] = result

        return {'machines':results, 'days_back':days_back}


# Finally, expose all we have defined through a single db instance.
def model_factory(request):
    return WhiningModel(settings.settings.get_db(settings.WMATRIX_DB_NAME),
                        request)
